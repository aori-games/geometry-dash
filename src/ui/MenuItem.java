package ui;

import jade.Window;
import jade.entity.Component;
import jade.entity.Entity;
import jade.entity.component.LevelEditorControls;
import jade.entity.component.Sprite;
import jade.scene.LevelEditorScene;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class MenuItem extends Component {
    private int x, y, width, height;
    private Sprite buttonSprite, hoverSprite, myImage;

    private boolean isSelected;


    private int bufferX, bufferY;

    private MainContainer parentContainer;
    public MenuItem(int x, int y, int width, int height, Sprite buttonSprite, Sprite coverSprite, MainContainer parentContainer) {
        this.x = x;
        this.parentContainer = parentContainer;
        this.y = y;
        this.width = width;
        this.height = height;
        this.buttonSprite = buttonSprite;
        this.hoverSprite = coverSprite;
        this.isSelected = false;
    }
    @Override
    public void start() {
        myImage = getEntity().getComponent(Sprite.class);
        this.bufferX = (this.width / 2) - (myImage.getWidth() / 2);
        this.bufferY = (this.height / 2) - (myImage.getHeight() / 2);
    }

    @Override
    public void update(double dt) {
        // IF THE CURSOR IS WITHIN THE BUTTON RANGE
        if (Window.getWindow().getMouseListener().getMouseButton() == MouseEvent.BUTTON1 && !isSelected && (Window.getWindow().getMouseListener().getX() > this.x && Window.getWindow().getMouseListener().getX() <= this.x + this.width
                && Window.getWindow().getMouseListener().getY() > this.y && Window.getWindow().getMouseListener().getY() <= this.y + this.height
                && Window.getWindow().getMouseListener().isMousePressed())) {
            isSelected = true;
            Entity obj = getEntity().copy();
            obj.removeComponent(MenuItem.class);
            LevelEditorScene scene = (LevelEditorScene) Window.getWindow().getCurrentScene();
            LevelEditorControls levelEditorControls = scene.getMouseCursor().getComponent(LevelEditorControls.class);
            obj.addComponent(levelEditorControls);
            scene.setMouseCursor(obj);
            this.parentContainer.setHotButton(getEntity());
        }

        if(Window.keyListener().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            isSelected = false;
        }
    }

    @Override
    public MenuItem copy() {
        return new MenuItem(this.x, this.y, this.width, this.height, (Sprite) this.buttonSprite.copy(), (Sprite) this.hoverSprite.copy(), this.parentContainer);
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.drawImage(buttonSprite.getImage(), this.x, this.y, this.width, this.height, null);
        g2.drawImage(myImage.getImage(), this.x + bufferX, this.y + bufferY, myImage.getWidth(), myImage.getHeight(), null);

        if (isSelected) {
            g2.drawImage(hoverSprite.getImage(), this.x, this.y, this.width, this.height, null);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
