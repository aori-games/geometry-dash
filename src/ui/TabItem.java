package ui;

import jade.Window;
import jade.entity.Component;
import jade.entity.component.Sprite;

import java.awt.*;
import java.awt.event.MouseEvent;

public class TabItem extends Component {
    private int x, y, width, height;
    private Sprite sprite;

    private boolean isSelected;

    private int bufferX, bufferY;

    private MainContainer parentContainer;
    public TabItem(int x, int y, int width, int height, Sprite sprite, MainContainer parentContainer) {
        this.x = x;
        this.parentContainer = parentContainer;
        this.y = y;
        this.width = width;
        this.height = height;
        this.sprite = sprite;
        this.isSelected = false;
    }
    @Override
    public void update(double delta) {
        // IF THE CURSOR IS WITHIN THE BUTTON RANGE
        if (Window.getWindow().getMouseListener().getMouseButton() == MouseEvent.BUTTON1 && !isSelected && (Window.getWindow().getMouseListener().getX() > this.x && Window.getWindow().getMouseListener().getX() <= this.x + this.width
                && Window.getWindow().getMouseListener().getY() > this.y && Window.getWindow().getMouseListener().getY() <= this.y + this.height
                && Window.getWindow().getMouseListener().isMousePressed())) {
            isSelected = true;
            this.parentContainer.setHotTab(getEntity());
        }
    }

    @Override
    public void draw(Graphics2D g2d) {
        if (isSelected) {
            g2d.drawImage(sprite.getImage(), x, y, width, height, null);
        } else {
            float alpha = 0.5f;
            AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2d.setComposite(ac);
            g2d.drawImage(sprite.getImage(), x, y, width, height, null);
            alpha = 1.0f;
            ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2d.setComposite(ac);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
