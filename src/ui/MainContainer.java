package ui;

import jade.Window;
import jade.data.Constants;
import jade.dataStructure.AssetPool;
import jade.dataStructure.Transform;
import jade.entity.Component;
import jade.entity.Entity;
import jade.entity.component.*;
import util.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainContainer extends Component {
    public Sprite containerBg;
    public List<Entity> tabs;
    public Map<Entity, List<Entity>> tabMaps;
    List<Entity> menuItems;
    private Entity hotButton = null;
    private Entity hotTab = null;

    public MainContainer() {
        this.menuItems = new ArrayList<>();
        this.tabs = new ArrayList<>();
        this.tabMaps = new HashMap<>();

        this.containerBg = AssetPool.getSprite("src/assets/ui/menuContainerBackground.png");
        init();
    }

    public void init() {
        SpriteSheet tabSprites = AssetPool.getSpriteSheet("src/assets/ui/tabs.png");

        for(int i = 0; i < tabSprites.getSprites().size(); ++i) {
            Sprite currentTab = tabSprites.getSprites().get(i);
            int x = Constants.TAB_OFFSET_X + (currentTab.column * Constants.TAB_WIDTH) + (currentTab.column * Constants.TAB_HORIZONTAL_SPACING);
            int y = Constants.TAB_OFFSET_Y;

            Entity entity = new Entity("Tab", new Transform(new Vector2(x,y)),10);
            entity.setUI(true);
            entity.setNoneSerializable();
            TabItem item = new TabItem(x,y, Constants.TAB_WIDTH, Constants.TAB_HEIGHT, currentTab, this);
            entity.addComponent(item);

            this.tabs.add(entity);
            this.tabMaps.put(entity,new ArrayList<>());

            Window.getWindow().getCurrentScene().addEntity(entity);
        }
        hotTab = this.tabs.get(0);
        this.hotTab.getComponent(TabItem.class).setSelected(true);

        addTabObjects();
    }

    private void addTabObjects() {
        SpriteSheet groundSprite = AssetPool.getSpriteSheet("src/assets/groundSprites.png");
        SpriteSheet buttonSprite = AssetPool.getSpriteSheet("src/assets/ui/buttonSprites.png");
        SpriteSheet spikeSprites = AssetPool.getSpriteSheet("src/assets/spikes.png");
        SpriteSheet bigSprites = AssetPool.getSpriteSheet("src/assets/bigSprites.png");
        SpriteSheet smallBlocks = AssetPool.getSpriteSheet("src/assets/smallBlocks.png");
        SpriteSheet portalSprites = AssetPool.getSpriteSheet("src/assets/portal.png");

        for (int i = 0; i < groundSprite.getSprites().size(); i++) {
            Sprite currentSprite = groundSprite.getSprites().get(i);
            int x = Constants.BUTTON_OFFSET_X + (currentSprite.column * Constants.BUTTON_WIDTH) +
                    (currentSprite.column * Constants.BUTTON_SPACING_HZ);
            int y = Constants.BUTTON_OFFSET_Y + (currentSprite.row * Constants.BUTTON_HEIGHT) +
                    (currentSprite.row * Constants.BUTTON_SPACING_VT);

            //Add first tab container objs
            Entity obj = new Entity("Generated", new Transform(new Vector2(x, y)),10);
            obj.setUI(true);
            obj.setNoneSerializable();
            obj.addComponent(currentSprite.copy());
            MenuItem menuItem = new MenuItem(x, y, Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT, buttonSprite.getSprites().get(0), buttonSprite.getSprites().get(1), this);
            obj.addComponent(menuItem);
            obj.addComponent(new BoxBounds(Constants.TILE_WIDTH, Constants.TILE_HEIGHT));
            this.tabMaps.get(this.tabs.get(0)).add(obj);

            //Add second tab container objs
            if(i < smallBlocks.getSprites().size()) {
                obj = new Entity("Generated", new Transform(new Vector2(x, y)),10);
                obj.setUI(true);
                obj.setNoneSerializable();
                menuItem = menuItem.copy();
                obj.addComponent(smallBlocks.getSprites().get(i));
                obj.addComponent(menuItem);

                if(i == 0) {
                    BoxBounds boxBounds = new BoxBounds(Constants.TILE_WIDTH, 16);
                    boxBounds.yBuffer = 42 - 16;
                    obj.addComponent(boxBounds);
                }
                this.tabMaps.get(tabs.get(1)).add(obj);
            }

            //Add fourth tab container objs
            if(i < spikeSprites.getSprites().size()) {
                obj = new Entity("Generated", new Transform(new Vector2(x, y)),10);
                obj.setUI(true);
                obj.setNoneSerializable();
                menuItem = menuItem.copy();
                obj.addComponent(spikeSprites.getSprites().get(i));
                obj.addComponent(menuItem);
                obj.addComponent(new TriangleBounds(42,42));
                this.tabMaps.get(tabs.get(3)).add(obj);
            }

            //Add fifth tab container obj
            if(i == 0) {
                obj = new Entity("Generated",new Transform(new Vector2(x, y)),10);
                obj.setUI(true);
                obj.setNoneSerializable();
                menuItem = menuItem.copy();
                obj.addComponent(menuItem);
                obj.addComponent(bigSprites.getSprites().get(i));
                obj.addComponent(new BoxBounds(Constants.TILE_WIDTH * 2, 56));
                this.tabMaps.get(tabs.get(4)).add(obj);
            }

            //Add sixth tab container obj
            if(i < portalSprites.getSprites().size()) {
                obj = new Entity("Generated", new Transform(new Vector2(x,y)),10);
                obj.setUI(true);
                obj.setNoneSerializable();
                menuItem = menuItem.copy();
                obj.addComponent(menuItem);
                obj.addComponent(portalSprites.getSprites().get(i));
                obj.addComponent(new BoxBounds(44,85, true));
                // TODO : Create portalComponenet here
                if(i == 0) {
                    obj.addComponent(new Portal(PlayerState.FLYING));

                } else if(i == 1) {
                    obj.addComponent(new Portal(PlayerState.NORMAL));
                }
                this.tabMaps.get(tabs.get(5)).add(obj);
            }
        }
    }

    @Override
    public void start() {
        for (Entity entity : tabs) {
            for(Entity entity1 : tabMaps.get(entity)) {
                for (Component c : entity1.getAllComponents()) {
                    c.start();
                }
            }
        }
    }

    @Override
    public void update(double dt) {
        for (Entity entity : tabMaps.get(hotTab)) {
            entity.update(dt);

            MenuItem menuItem = entity.getComponent(MenuItem.class);
            if( entity != hotButton && menuItem.isSelected()) {
                menuItem.setSelected(false);
            }
        }

        for(Entity entity: this.tabs) {
            TabItem tabItem = entity.getComponent(TabItem.class);
            if(entity != hotTab && tabItem.isSelected()) {
                tabItem.setSelected(false);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.drawImage(containerBg.getImage(), 0, Constants.CONTAINER_OFFSET_Y, this.containerBg.getWidth(), this.containerBg.getHeight(), null);
        for (Entity entity : tabMaps.get(hotTab)) {
            entity.draw(g2);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }

    public void setHotButton(Entity entity) {
        this.hotButton = entity;
    }

    public void setHotTab(Entity entity) {
        this.hotTab = entity;
    }
}
