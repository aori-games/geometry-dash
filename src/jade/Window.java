package jade;

import jade.data.Constants;
import jade.listener.KL;
import jade.listener.ML;
import jade.scene.LevelEditorScene;
import jade.scene.LevelScene;
import jade.scene.Scene;
import util.Time;

import javax.swing.JFrame;
import java.awt.*;

public class Window extends JFrame implements Runnable {
    private ML mouseListener;
    private KL keyListener;
    private boolean isRunning = true;
    private Scene currentScene = null;
    private Image dbImage = null;
    private Graphics dbGraphics = null;
    private boolean isInEditor = true;
    private static Window window = null; //Singleton


    public Window() {
        this.mouseListener = new ML();
        this.keyListener = new KL();

        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setTitle(Constants.SCREEN_TITLE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.addKeyListener(keyListener);
        this.addMouseListener(mouseListener);
        this.addMouseMotionListener(mouseListener);
    }

    public static Window getWindow() {
        if (window == null) {
            window = new Window();
        }
        return Window.window;
    }

    public Scene getCurrentScene() {
        return currentScene;
    }

    public void init() {
        changeScene(0);
    }

    public void changeScene(int scene) {
        switch (scene) {
            case 0:
                isInEditor = true;
                currentScene = new LevelEditorScene("Level Editor");
                currentScene.init();
                break;
            case 1:
                isInEditor = false;
                currentScene = new LevelScene("Level scene");
                currentScene.init();
                break;
            default:
                System.out.println("Invalid scene number");
                currentScene = new LevelEditorScene("Level Editor");
                break;
        }
    }

    public void update(double dt) {
        currentScene.update(dt);
        draw(getGraphics());
    }

    public void draw(Graphics g) {
        if(dbImage == null) {
            dbImage = createImage(this.getWidth(), this.getHeight());
            dbGraphics = dbImage.getGraphics();
        }
        renderOffScreen(dbGraphics);
        g.drawImage(dbImage, 0, 0, this.getWidth(), this.getHeight(), null);
    }

    public void renderOffScreen(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        currentScene.draw(g2d);
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;

        try {
            while (isRunning) {
                double time = Time.getTime();
                double deltaTime = time - lastFrameTime;
                lastFrameTime = time;

                update(deltaTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Scene getScene() {
        return Window.getWindow().getCurrentScene();
    }

    public static ML mouseListener() {
        return getWindow().getMouseListener();
    }

    public static KL keyListener() {
        return getWindow().getKeyListener();
    }

    //GETTERS & SETTERS
    public boolean isInEditor() {
        return isInEditor;
    }
    public ML getMouseListener() {
        return mouseListener;
    }
    public KL getKeyListener() {
        return keyListener;
    }
}
