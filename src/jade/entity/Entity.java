package jade.entity;

import jade.dataStructure.Transform;
import jade.file.Parser;
import jade.file.Serialize;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Entity extends Serialize {
    private List<Component> components;
    private String name;
    private Transform transform;
    public boolean serializable = true;
    private boolean isUI = false;
    public int zIndex;

    public Entity(String name, Transform transform, int zIndex) {
        this.name = name;
        this.zIndex = zIndex;
        this.transform = transform;
        this.components = new ArrayList<Component>();

    }

    public <T extends Component> T getComponent(Class<T> componentClass) {
        for (Component component : components) {
            if(componentClass.isAssignableFrom(component.getClass())) {
                try {
                    return componentClass.cast(component);
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }

        return null;
    }
    public <T extends Component> void removeComponent(Class<T> componentClass) {
        for (Component component : components) {
            if(componentClass.isAssignableFrom(component.getClass())) {
                components.remove(component);
                return;
            }
        }
    }

    public void update(double delta) {
        for (Component component : components) {
            component.update(delta);
        };
    }

    public void addComponent(Component c) {
        components.add(c);
        c.setEntity(this);
    }

    public List<Component> getAllComponents() {
        return components;
    }



    public void draw(Graphics2D g2) {
        for (Component component : components) {
            component.draw(g2);
        }
    }

    public Entity copy() {
        Entity newEntity = new Entity("Generated", transform.copy(),this.zIndex);
        for (Component component : components) {
            Component copy = component.copy();
            if(copy != null) {
                newEntity.addComponent(copy);
            }
        }
        return newEntity;
    }

    public void setNoneSerializable() {
        serializable = false;
    }

    @Override
    public String serialize(int tabSize) {
        if(!serializable) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(beginObjectProperty("Entity", tabSize));

        //Transform
        builder.append(transform.serialize(tabSize + 1));
        builder.append(addEnding(true, true));
        builder.append(addStringProperty("Name", name, tabSize + 1, true, true));

        if(components.size() > 0) {
            builder.append(addIntProperty("zIndex", this.zIndex, tabSize + 1, true, true));
            builder.append(beginObjectProperty("Components", tabSize + 1));

        } else {
            builder.append(addIntProperty("zIndex", this.zIndex,tabSize + 1, true, false));
        }

        int i = 0;
        for (Component component : components) {
            String str = component.serialize(tabSize + 2);
            if(str.compareTo("") != 0) {
                builder.append(str);
                if(i != components.size() - 1) {
                    builder.append(addEnding(true, true));
                } else  {
                    builder.append(addEnding(true, false));
                }
            }
            i++;
        }

        if(components.size() > 0) {
            builder.append(closeObjectProperty(tabSize + 1));
            builder.append(addEnding(true, false));
            builder.append(closeObjectProperty(tabSize));
        }
        return builder.toString();
    }

    public static Entity deserialize() {
        Parser.consumeBeginObjectProperty("Entity");
        Transform transform =  Transform.deserialize();
        Parser.consume(',');
        String name = Parser.consumeStringProperty("Name");
        Parser.consume(',');
        int zIndex = Parser.consumeIntProperty("zIndex");

        Entity newEntity = new Entity(name, transform, zIndex);
        if(Parser.peek() == ',') {
            Parser.consume(',');
            Parser.consumeBeginObjectProperty("Components");
            newEntity.addComponent(Parser.parseComponent());

            while (Parser.peek() == ',') {
                Parser.consume(',');
                newEntity.addComponent(Parser.parseComponent());
            }
            Parser.consumeEndObjectProperty();
        }
        Parser.consumeEndObjectProperty();
        return newEntity;
    }

    // GETTERS & SETTERS
    public Transform getTransform() {
        return transform;
    }

    public void setTransform(Transform transform) {
        this.transform = transform;
    }

    public boolean isUI() {
        return isUI;
    }

    public void setUI(boolean UI) {
        isUI = UI;
    }
}
