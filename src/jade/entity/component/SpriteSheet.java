package jade.entity.component;

import jade.dataStructure.AssetPool;

import java.util.ArrayList;
import java.util.List;

public class SpriteSheet {

    private List<Sprite> sprites;
    private int tileWidth, tileHeight, spacing, columns, size;

    public SpriteSheet(String pictureFile, int tileWidth, int tileHeight, int spacing, int columns, int size) {
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.spacing = spacing;
        this.columns = columns;
        this.size = size;

        Sprite parent = AssetPool.getSprite(pictureFile);
        sprites = new ArrayList<Sprite>();
        int row = 0;
        int count = 0;
        //Getting all the sub image from the sprite sheet img
        while (count < size) {
            for (int column = 0; column < columns; column++) {
                int imgX = (column * tileWidth) + (column * spacing);
                int imgY = (row * tileHeight) + (row * spacing);

                sprites.add(new Sprite(parent.getImage().getSubimage(imgX, imgY, tileWidth, tileHeight),
                        row, column, count, pictureFile));
                count++;
                if (count > size - 1) {
                    break;
                }
            }
            row++;
        }

    }

    // GETTERS && SETTERS

    public List<Sprite> getSprites() {
        return sprites;
    }
}
