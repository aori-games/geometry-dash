package jade.entity.component;

import jade.Window;
import jade.entity.Component;

import java.awt.event.MouseEvent;

public class CameraControls extends Component {

    private float prevMx, prevMy;

    public CameraControls() {
        prevMx = 0.0f;
        prevMy = 0.0f;
    }

    @Override
    public void update(double dt) {
        if(Window.getWindow().getMouseListener().isMousePressed() && Window.getWindow().getMouseListener().getMouseButton() == MouseEvent.BUTTON2) {
            float dx = (Window.getWindow().getMouseListener().getX() + Window.getWindow().getMouseListener().getDx() - prevMx);
            float dy = (Window.getWindow().getMouseListener().getY() + Window.getWindow().getMouseListener().getDy() - prevMy);

            Window.getWindow().getCurrentScene().getCamera().getPosition().x -= dx;
            Window.getWindow().getCurrentScene().getCamera().getPosition().y -= dy;
        }

        prevMx = Window.getWindow().getMouseListener().getX()  + Window.getWindow().getMouseListener().getDx();
        prevMy = Window.getWindow().getMouseListener().getY() + Window.getWindow().getMouseListener().getDy();
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
