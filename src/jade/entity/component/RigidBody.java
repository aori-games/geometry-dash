package jade.entity.component;

import jade.data.Constants;
import jade.entity.Component;
import util.Vector2;

public class RigidBody extends Component {

    public Vector2 velocity;

    public RigidBody(Vector2 velocity) {
        this.velocity = velocity;
    }

    @Override
    public void update(double dt) {
        getEntity().getTransform().getPosition().y += velocity.y * dt;
        getEntity().getTransform().getPosition().x += velocity.x * dt;

        velocity.y += (float) (Constants.GRAVITY * dt);

        if(Math.abs(velocity.y) > Constants.TERMINAL_VELOCITY) {
            velocity.y = Math.signum(velocity.y) * Constants.TERMINAL_VELOCITY;
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
