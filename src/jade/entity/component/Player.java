package jade.entity.component;

import jade.Window;
import jade.data.Constants;
import jade.dataStructure.AssetPool;
import jade.entity.Component;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;

public class Player extends Component {

    private final Sprite layerOne, layerTwo, layerThree, spaceShip;
    private final int playerWidth, playerHeight;
    public PlayerState playerState;

    private boolean onGround = true;

    public Player(Sprite layerOne, Sprite layerTwo, Sprite layerThree, Color colorOne, Color colorTwo) {
        this.layerOne = layerOne;
        this.layerTwo = layerTwo;
        this.layerThree = layerThree;
        this.spaceShip = AssetPool.getSprite("src/assets/player/spaceship.png");
        this.playerWidth = Constants.PLAYER_WIDTH;
        this.playerHeight = Constants.PLAYER_HEIGHT;
        this.playerState = PlayerState.NORMAL;

        int threshold = 200;
        int indexLayer = 0;
        for (Sprite layer : new Sprite[]{this.layerOne, this.layerTwo, this.layerThree}) {
            for (int y = 0; y < layer.getImage().getWidth(); y++) {
                for (int x = 0; x < layer.getImage().getHeight(); x++) {
                    Color color = new Color(layer.getImage().getRGB(x, y));
                    if (color.getRed() > threshold && color.getBlue() > threshold && color.getGreen() > threshold) {
                        if (indexLayer != 1) {
                            layer.getImage().setRGB(x, y, colorOne.getRGB());
                        } else {
                            layer.getImage().setRGB(x, y, colorTwo.getRGB());
                        }
                    }
                }
            }
            indexLayer++;
        }
    }

    @Override
    public void update(double dt) {
        if(onGround && Window.getWindow().getKeyListener().isKeyPressed(KeyEvent.VK_SPACE)) {
            if(playerState == PlayerState.NORMAL) {
                addJumpForce();
            }
            onGround = false;
        }

        if(playerState == PlayerState.FLYING && Window.getWindow().getKeyListener().isKeyPressed(KeyEvent.VK_SPACE)) {
            addFlyingForce();
            this.onGround = false;
        }

        if(playerState != PlayerState.FLYING && !onGround) {
            getEntity().getTransform().setRotation((float) (getEntity().getTransform().getRotation() + 10.0f * dt));
        } else  if(playerState != PlayerState.FLYING){
            getEntity().getTransform().setRotation(getEntity().getTransform().getRotation() % 360.0f);
            if(getEntity().getTransform().getRotation() > 180.0f && getEntity().getTransform().getRotation() < 360.0f) {
                getEntity().getTransform().setRotation(0);
            } else if(getEntity().getTransform().getRotation() > 0.0f && getEntity().getTransform().getRotation() < 180f) {
                getEntity().getTransform().setRotation(0);
            }
        }
    }

    public void addFlyingForce() {
        getEntity().getComponent(RigidBody.class).velocity.y = Constants.JUMP_FORCE;
    }

    private void addJumpForce() {
        getEntity().getComponent(RigidBody.class).velocity.y = Constants.JUMP_FORCE;
    }

    public void die() {
        getEntity().getTransform().getPosition().x = 500;
        getEntity().getTransform().getPosition().y = 350;
        getEntity().getComponent(RigidBody.class).velocity.y = 0;
        getEntity().getTransform().setRotation(0);
        Window.getWindow().getCurrentScene().getCamera().getPosition().x = 0;
    }

    @Override
    public void draw(Graphics2D g2) {
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.setToIdentity();
        affineTransform.translate(getEntity().getTransform().getPosition().x, getEntity().getTransform().getPosition().y);
        affineTransform.rotate(getEntity().getTransform().getRotation(), playerWidth * getEntity().getTransform().getScale().x / 2.0, playerHeight * getEntity().getTransform().getScale().y / 2.0);
        affineTransform.scale(getEntity().getTransform().getScale().x, getEntity().getTransform().getScale().y);

        if(playerState == PlayerState.NORMAL) {
            for (Sprite sprite : new Sprite[]{this.layerOne, this.layerTwo, this.layerThree}) {
                g2.drawImage(sprite.getImage(), affineTransform, null);
            }
        } else {
            affineTransform.setToIdentity();
            affineTransform.translate(getEntity().getTransform().getPosition().x, getEntity().getTransform().getPosition().y);
            affineTransform.rotate(getEntity().getTransform().getRotation(), playerWidth * getEntity().getTransform().getScale().x / 4.0, playerHeight * getEntity().getTransform().getScale().y / 4.0);
            affineTransform.scale(getEntity().getTransform().getScale().x / 2, getEntity().getTransform().getScale().y / 2);
            affineTransform.translate(15,15);
            for (Sprite sprite : new Sprite[]{this.layerOne, this.layerTwo, this.layerThree}) {
                g2.drawImage(sprite.getImage(), affineTransform, null);
            }

            affineTransform.setToIdentity();
            affineTransform.translate(getEntity().getTransform().getPosition().x, getEntity().getTransform().getPosition().y);
            affineTransform.rotate(getEntity().getTransform().getRotation(), playerWidth * getEntity().getTransform().getScale().x / 2.0, playerHeight * getEntity().getTransform().getScale().y / 2.0);
            affineTransform.scale(getEntity().getTransform().getScale().x, getEntity().getTransform().getScale().y);
            g2.drawImage(spaceShip.getImage(), affineTransform, null);

        }

    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }
}
