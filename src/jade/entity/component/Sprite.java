package jade.entity.component;

import jade.dataStructure.AssetPool;
import jade.entity.Component;
import jade.file.Parser;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

public class Sprite extends Component {
    private BufferedImage image;
    private String pictureFile;
    private int width, height;

    public boolean isSubSprite = false;
    public int row, column, index;

    public Sprite(String pictureFile) {
        this.pictureFile = pictureFile;

        try {
            File file = new File(pictureFile);
            if(AssetPool.hasSprite(pictureFile)) {
                throw new Exception("Asset already exists : " + pictureFile);
            }
            this.image = ImageIO.read(file);
            this.width = image.getWidth();
            this.height = image.getHeight();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public Sprite(BufferedImage image, String pictureFile) {
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.pictureFile = pictureFile;
    }

    public Sprite(BufferedImage image, int row, int column, int index, String pictureFile) {
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.row = row;
        this.column = column;
        this.index = index;
        this.isSubSprite = true;
        this.pictureFile = pictureFile;
    }

    @Override
    public void draw(Graphics2D g) {
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.setToIdentity();
        affineTransform.translate(getEntity().getTransform().getPosition().x, getEntity().getTransform().getPosition().y);
        affineTransform.rotate(Math.toRadians(getEntity().getTransform().getRotation()), this.width * getEntity().getTransform().getScale().x / 2.0, this.height * getEntity().getTransform().getScale().y / 2.0);
        affineTransform.scale(getEntity().getTransform().getScale().x, getEntity().getTransform().getScale().y);
        g.drawImage(image, affineTransform, null);
    }

    @Override
    public Component copy() {
        if(!isSubSprite) {
            return new Sprite(this.image, this.pictureFile);
        }
        return new Sprite(this.image, row, column, index, pictureFile);
    }

    public static Sprite deserialize() {
        boolean isSubSprite = Parser.consumeBooleanProperty("isSubSprite");
        Parser.consume(',');
        String filePath = Parser.consumeStringProperty("FilePath");

        if(isSubSprite) {
            Parser.consume(',');
            Parser.consumeIntProperty("Row");
            Parser.consume(',');
            Parser.consumeIntProperty("Column");
            Parser.consume(',');
            int index = Parser.consumeIntProperty("Index");
            if(!AssetPool.hasSprite(filePath)) {
                System.out.println("SpriteSheet " + filePath + " not loaded yet");
                System.exit(-1);
            }
            Parser.consumeEndObjectProperty();
            return (Sprite) AssetPool.getSpriteSheet(filePath).getSprites().get(index).copy();
        }

        if(!AssetPool.hasSprite(filePath)){
            System.out.println("SpriteSheet " + filePath + " not loaded yet");
            System.exit(-1);
        }

        Parser.consumeEndObjectProperty();
        return (Sprite) AssetPool.getSprite(filePath).copy();
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder builder = new StringBuilder();
        builder.append(beginObjectProperty("Sprite", tabSize));
        builder.append(addBooleanProperty("isSubSprite", isSubSprite,tabSize + 1, true,true));
        if(isSubSprite) {
            builder.append(addStringProperty("FilePath", pictureFile, tabSize + 1 , true, true));
            builder.append(addIntProperty("Row", row, tabSize + 1, true, true));
            builder.append(addIntProperty("Column", column, tabSize + 1, true, true));
            builder.append(addIntProperty("Index", index, tabSize + 1, true, false));
            builder.append(closeObjectProperty(tabSize));
            return builder.toString();
        }
        builder.append(addBooleanProperty("isSubSprite", isSubSprite,tabSize+1, true,false));
        builder.append(closeObjectProperty(tabSize));
        return builder.toString();

    }
    // GETTERS && SETTERS

    public String getPictureFile() {
        return pictureFile;
    }
    public void setPictureFile(String pictureFile) {
        this.pictureFile = pictureFile;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
