package jade.entity.component;

import jade.data.Constants;
import jade.entity.Component;
import jade.entity.Entity;
import jade.file.Parser;
import util.Vector2;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class BoxBounds extends Bounds {
    private float width;
    private float height;
    public float halfWidth, halfHeight;
    private Vector2 center = new Vector2();
    public float enclosingRadius;
    public boolean isTrigger;
    public float xBuffer = 0.0f;
    public float yBuffer = 0.0f;

    public BoxBounds(float width, float height) {
        init(width, height, false);
    }

    public BoxBounds(float width, float height, boolean isTrigger) {
        init(width, height, isTrigger);
    }

    public void init(float width, float height, boolean trigger) {
        this.width = width;
        this.height = height;
        this.halfWidth = width / 2.0f;
        this.halfHeight = height / 2.0f;
        this.type = BoundsType.Box;
        this.isTrigger = trigger;
        this.enclosingRadius = (float) Math.sqrt((this.halfWidth * this.halfWidth) + (this.halfHeight * this.halfHeight));
    }

    public void resolveCollision(Entity player) {
        if (isTrigger) return;

        BoxBounds playerBounds = player.getComponent(BoxBounds.class);
        playerBounds.calculateCenter();
        this.calculateCenter();

        float dx = this.center.x - playerBounds.center.x;
        float dy = this.center.y - playerBounds.center.y;

        float combinedHalfWidths = playerBounds.halfWidth + this.halfWidth;
        float combinedHalfHeights = playerBounds.halfHeight + this.halfHeight;

        float overlapX = combinedHalfWidths - Math.abs(dx);
        float overlapY = combinedHalfHeights - Math.abs(dy);

        if (overlapX >= overlapY) {
            if (dy > 0) {
                // Collision on the top of the player
                player.getTransform().getPosition().y = getEntity().getTransform().getPosition().y - playerBounds.getHeight() + yBuffer;
                player.getComponent(RigidBody.class).velocity.y = 0;
                player.getComponent(Player.class).setOnGround(true);
            } else {
                // Collision on the bottom of the player
                player.getComponent(Player.class).die();
            }
        } else {
            // Collision on the left or right of the player
            if (dx < 0 && dy <= 0.3) {
                player.getTransform().getPosition().y = getEntity().getTransform().getPosition().y - playerBounds.getHeight() + yBuffer;
                player.getComponent(RigidBody.class).velocity.y = 0;
                player.getComponent(Player.class).setOnGround(true);
            } else {
                player.getComponent(Player.class).die();
            }
        }
    }

    @Override
    public void start() {
        this.calculateCenter();
    }


    @Override
    public void update(double dt) {

    }

    @Override
    public Component copy() {
        BoxBounds bounds = new BoxBounds(width, height, isTrigger);
        bounds.xBuffer = xBuffer;
        bounds.yBuffer = yBuffer;
        return bounds;
    }

    private void calculateCenter() {
        this.center.x = this.getEntity().getTransform().getPosition().x + halfWidth + this.xBuffer;
        this.center.y = this.getEntity().getTransform().getPosition().y + halfHeight + this.yBuffer;
    }

    public static boolean checkCollision(BoxBounds b1, BoxBounds b2) {
        b1.calculateCenter();
        b2.calculateCenter();

        float dx = b2.center.x - b1.center.x;
        float dy = b2.center.y - b1.center.y;

        float combinedHalfWidths = b1.halfWidth + b2.halfWidth;
        float combinedHalfHeights = b1.halfHeight + b2.halfHeight;

        if (Math.abs(dx) <= combinedHalfWidths) {
            return Math.abs(dy) <= combinedHalfHeights;
        }

        return false;
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder builder = new StringBuilder();
        builder.append(beginObjectProperty("BoxBounds", tabSize));
        builder.append(addFloatProperty("Width", this.width, tabSize + 1, true, true));
        builder.append(addFloatProperty("Height", this.height, tabSize + 1, true, true));
        builder.append(addFloatProperty("xBuffer", this.xBuffer, tabSize + 1, true, true));
        builder.append(addFloatProperty("yBuffer", this.yBuffer, tabSize + 1, true, true));
        builder.append(addBooleanProperty("isTrigger", this.isTrigger, tabSize + 1, true, false));
        builder.append(closeObjectProperty(tabSize));
        return builder.toString();
    }

    public static BoxBounds deserialize() {
        float width = Parser.consumeFloatProperty("Width");
        Parser.consume(',');
        float height = Parser.consumeFloatProperty("Height");
        Parser.consume(',');
        float xBuffer = Parser.consumeFloatProperty("xBuffer");
        Parser.consume(',');
        float yBuffer = Parser.consumeFloatProperty("yBuffer");
        Parser.consume(',');
        boolean isTrigger = Parser.consumeBooleanProperty("isTrigger");
        Parser.consumeEndObjectProperty();
        BoxBounds bounds = new BoxBounds(width, height, isTrigger);
        bounds.xBuffer = xBuffer;
        bounds.yBuffer = yBuffer;
        return bounds;
    }
    // GETTERS & SETTERS

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    @Override
    public boolean raycast(Vector2 position) {
        return position.x > this.getEntity().getTransform().getPosition().x + xBuffer && position.x < this.getEntity().getTransform().getPosition().x + getWidth() + xBuffer
                && position.y > this.getEntity().getTransform().getPosition().y + yBuffer && position.y < this.getEntity().getTransform().getPosition().y + getHeight() + yBuffer;
    }

    public void setCenter(Vector2 center) {
        this.center = center;
    }

    @Override
    public void draw(Graphics2D g2d) {
        if (isSelected) {
            g2d.setColor(Color.GREEN);
            g2d.setStroke(Constants.THICK_LINE);
            g2d.draw(new Rectangle2D.Float(this.getEntity().getTransform().getPosition().x + xBuffer,
                    this.getEntity().getTransform().getPosition().y + yBuffer, this.width, this.height));
            g2d.setStroke(Constants.LINE);
        }
    }
}
