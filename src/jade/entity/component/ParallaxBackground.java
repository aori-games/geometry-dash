package jade.entity.component;

import jade.Window;
import jade.data.Constants;
import jade.dataStructure.AssetPool;
import jade.entity.Component;
import jade.entity.Entity;

import java.awt.*;

public class ParallaxBackground extends Component {
    public int width, height;
    public Sprite sprite;
    public Entity[] backgrounds;
    public int timeStep = 0;
    private float speed = 80.f;

    private Ground ground;
    private boolean followGround;

    public ParallaxBackground(String file, Entity[] backgrounds, Ground ground, boolean followGround) {
        this.sprite = AssetPool.getSprite(file);
        this.width = sprite.getWidth();
        this.height = sprite.getHeight();
        this.backgrounds = backgrounds;
        this.followGround = followGround;
        this.ground = ground;

        if (followGround) {
            this.speed = Constants.PLAYER_SPEED - 35;
        }
    }

    @Override
    public void update(double dt) {
        if (backgrounds == null) return;

        this.timeStep++;

        this.getEntity().getTransform().getPosition().x -= dt * speed;
        this.getEntity().getTransform().getPosition().x = (float)Math.floor(this.getEntity().getTransform().getPosition().x);
        if (this.getEntity().getTransform().getPosition().x < -width) {
            float maxX = 0;
            int otherTimeStep = 0;
            for (Entity go : backgrounds) {
                if (go.getTransform().getPosition().x > maxX) {
                    maxX = go.getTransform().getPosition().x;
                    otherTimeStep = go.getComponent(ParallaxBackground.class).timeStep;
                }
            }

            if (otherTimeStep == this.timeStep) {
                this.getEntity().getTransform().getPosition().x = maxX + width;
            } else {
                this.getEntity().getTransform().getPosition().x = (float) Math.floor((maxX + width) - (dt * speed));
            }
        }

        if (this.followGround) {
            this.getEntity().getTransform().getPosition().y = ground.getEntity().getTransform().getPosition().y;
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (followGround) {
            g2.drawImage(this.sprite.getImage(), (int)this.getEntity().getTransform().getPosition().x,
                    (int)(this.getEntity().getTransform().getPosition().y -
                            Window.getWindow().getCurrentScene().getCamera().getPosition().y),
                    width, height, null);
        } else {
            int height = Math.min((int)(ground.getEntity().getTransform().getPosition().y -
                            Window.getWindow().getCurrentScene().getCamera().getPosition().y),
                    Constants.SCREEN_HEIGHT);
            g2.drawImage(this.sprite.getImage(), (int)this.getEntity().getTransform().getPosition().x,
                    (int)this.getEntity().getTransform().getPosition().y, width, Constants.SCREEN_HEIGHT, null);
            g2.setColor(Constants.GROUND_COLOR);
            g2.fillRect((int)this.getEntity().getTransform().getPosition().x,
                    height, width, Constants.SCREEN_HEIGHT);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
