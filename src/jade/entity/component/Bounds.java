package jade.entity.component;

import jade.entity.Component;
import jade.entity.Entity;
import util.Vector2;

enum BoundsType {
    Box,
    Triangle
}

public abstract class Bounds extends Component {
    public BoundsType type;
    public boolean isSelected;

    public abstract float getWidth();
    public abstract float getHeight();
    public abstract boolean raycast(Vector2 position);

    public static boolean checkCollision(Bounds b1, Bounds b2) {
        if(b1.type == b2.type && b1.type == BoundsType.Box) {
            return BoxBounds.checkCollision((BoxBounds)b1, (BoxBounds)b2);
        } else if(b1.type == BoundsType.Box && b2.type == BoundsType.Triangle) {
            return TriangleBounds.checkCollision((BoxBounds)b1, (TriangleBounds)b2);
        } else if(b1.type == BoundsType.Triangle && b2.type == BoundsType.Box) {
            return TriangleBounds.checkCollision((BoxBounds)b2, (TriangleBounds)b1);
        }
        return false;
    }

    public static void resolveCollision(Bounds b, Entity player) {
        if(b.type == BoundsType.Box) {
            BoxBounds box = (BoxBounds)b;
            box.resolveCollision(player);
        } else if(b.type == BoundsType.Triangle) {
            player.getComponent(Player.class).die();
        }
    }
}
