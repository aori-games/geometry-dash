package jade.entity.component;

import jade.Window;
import jade.data.Constants;
import jade.entity.Component;
import jade.entity.Entity;
import jade.scene.LevelScene;
import java.awt.Color;
import java.awt.Graphics2D;

public class Ground extends Component {

    public Ground() {

    }

    @Override
    public void update(double dt) {
        if(!Window.getWindow().isInEditor()) {

            LevelScene scene = (LevelScene)(Window.getWindow().getCurrentScene());
            Entity player = scene.getPlayer();

            //IF PLAYER HIT THE GROUND
            if(player.getTransform().getPosition().y + player.getComponent(BoxBounds.class).getHeight() > getEntity().getTransform().getPosition().y) {
                player.getTransform().getPosition().y = getEntity().getTransform().getPosition().y - player.getComponent(BoxBounds.class).getHeight();
                player.getComponent(Player.class).setOnGround(true);
            }

            getEntity().getTransform().getPosition().x = scene.getCamera().getPosition().x;
        } else {
            getEntity().getTransform().getPosition().x = Window.getWindow().getCurrentScene().getCamera().getPosition().x;
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.black);
        g2.drawRect((int) getEntity().getTransform().getPosition().x - 10, (int) getEntity().getTransform().getPosition().y, Constants.SCREEN_WIDTH + 10, Constants.SCREEN_HEIGHT);
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
