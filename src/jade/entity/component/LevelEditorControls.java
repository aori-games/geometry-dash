package jade.entity.component;

import jade.Window;
import jade.data.Constants;
import jade.entity.Component;
import jade.entity.Entity;
import jade.scene.LevelEditorScene;
import util.Vector2;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

enum Direction {
    UP, DOWN, LEFT, RIGHT;
}

public class LevelEditorControls extends Component {
    private float debounceTime = 0.2f;
    private float debounceLeft = 0.0f;

    private float debounceKey = 0.2f;
    private float debounceKeyLeft = 0.0f;

    private boolean shiftKeyPressed = false;

    private List<Entity> selectedObjects;

    int gridWidth, gridHeight;
    private float worldX, worldY;
    private boolean isEditing = false;

    private boolean wasDragged = false;
    private float dragX, dragY, dragWidth, dragHeight;

    public LevelEditorControls(int gridWidth, int gridHeight) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.selectedObjects = new ArrayList<>();
    }

    public void updateSpritePosition() {
        this.worldX = (float)Math.floor((Window.getWindow().getMouseListener().getX() + Window.getWindow().getCurrentScene().getCamera().getPosition().x + Window.getWindow().getMouseListener().getDx()) / gridWidth);
        this.worldY= (float)Math.floor((Window.getWindow().getMouseListener().getY() + Window.getWindow().getCurrentScene().getCamera().getPosition().y + Window.getWindow().getMouseListener().getDy()) / gridWidth);
        this.getEntity().getTransform().getPosition().x = worldX * gridWidth - Window.getWindow().getCurrentScene().getCamera().getPosition().x;
        this.getEntity().getTransform().getPosition().y = worldY * gridWidth - Window.getWindow().getCurrentScene().getCamera().getPosition().y;
    }

    public void copyGameObjectToScene() {
        Entity object = getEntity().copy();
        object.getTransform().setPosition(new Vector2(this.worldX * gridWidth, this.worldY * gridHeight));
        Window.getWindow().getCurrentScene().addEntity(object);
    }

    public void addGameObjectToSelected(Vector2 mousePos) {
        mousePos.x += Window.getScene().getCamera().getPosition().x;
        mousePos.y += Window.getScene().getCamera().getPosition().y;
        for (Entity go : Window.getScene().getAllEntities()) {
            Bounds bounds = go.getComponent(Bounds.class);
            if (bounds != null && bounds.raycast(mousePos)) {
                selectedObjects.add(go);
                bounds.isSelected = true;
                break;
            }
        }
    }

    public List<Entity> boxCast(float x, float y, float width, float height) {
        float x0 = x + Window.getScene().getCamera().getPosition().x;
        float y0 = y + Window.getScene().getCamera().getPosition().y;

        List<Entity> objs = new ArrayList<>();
        for (Entity go : Window.getScene().getAllEntities()) {
            Bounds b = go.getComponent(Bounds.class);
            if (b != null) {
                if (go.getTransform().getPosition().x + b.getWidth() <= x0 + width &&
                        go.getTransform().getPosition().y + b.getHeight() <= y0 + height &&
                        go.getTransform().getPosition().x >= x0 && go.getTransform().getPosition().y >= y0) {
                    objs.add(go);
                }
            }
        }

        return objs;
    }

    public void clearSelectedObjectsAndAdd(Vector2 mousePos) {
        clearSelected();
        addGameObjectToSelected(mousePos);
    }

    public void escapeKeyPressed() {
        Entity newGameObj = new Entity("Mouse Cursor",
                this.getEntity().getTransform().copy(), this.getEntity().zIndex);
        newGameObj.addComponent(this);
        LevelEditorScene scene = (LevelEditorScene)Window.getScene();
        scene.setMouseCursor(newGameObj);
        isEditing = false;
    }

    public void clearSelected() {
        for (Entity go : selectedObjects) {
            go.getComponent(Bounds.class).isSelected = false;
        }
        selectedObjects.clear();
    }

    @Override
    public void update(double dt) {
        debounceLeft -= dt;
        debounceKeyLeft -= dt;

        if (!isEditing && this.getEntity().getComponent(Sprite.class) != null) {
            this.isEditing = true;
        }

        if (isEditing) {
            if (selectedObjects.size() != 0) {
                clearSelected();
            }
            updateSpritePosition();
        }


        if (Window.getWindow().getMouseListener().getY() < Constants.TAB_OFFSET_Y &&
                Window.getWindow().getMouseListener().isMousePressed() &&
                Window.getWindow().getMouseListener().getMouseButton() == MouseEvent.BUTTON1 &&
                debounceLeft < 0 && !wasDragged) {
            // Mouse has been clicked
            debounceLeft = debounceTime;

            if (isEditing) {
                copyGameObjectToScene();
            } else if (Window.keyListener().isKeyPressed(KeyEvent.VK_SHIFT)){
                addGameObjectToSelected(new Vector2(Window.mouseListener().getX(), Window.mouseListener().getY()));
            } else {
                clearSelectedObjectsAndAdd(new Vector2(Window.mouseListener().getX(), Window.mouseListener().getY()));
            }
        } else if (!Window.mouseListener().isMousePressed() && wasDragged) {
            wasDragged = false;
            clearSelected();
            List<Entity> objs = boxCast(dragX, dragY, dragWidth, dragHeight);
            for (Entity go : objs) {
                selectedObjects.add(go);
                Bounds b = go.getComponent(Bounds.class);
                if (b != null) {
                    b.isSelected = true;
                }
            }
        }

        if (Window.keyListener().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            escapeKeyPressed();
        }

        if (Window.keyListener().isKeyPressed(KeyEvent.VK_SHIFT)) {
            shiftKeyPressed = true;
        } else {
            shiftKeyPressed = false;
        }

        if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_LEFT)) {
            moveObjects(Direction.LEFT, shiftKeyPressed ? 0.1f : 1.0f);
            debounceKeyLeft = debounceKey;
        } else if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_RIGHT)) {
            moveObjects(Direction.RIGHT, shiftKeyPressed ? 0.1f : 1.0f);
            debounceKeyLeft = debounceKey;
        } else if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_UP)) {
            moveObjects(Direction.UP, shiftKeyPressed ? 0.1f : 1.0f);
            debounceKeyLeft = debounceKey;
        } else if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_DOWN)) {
            moveObjects(Direction.DOWN, shiftKeyPressed ? 0.1f : 1.0f);
            debounceKeyLeft = debounceKey;
        } else if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_DELETE)) {
            for (Entity go : selectedObjects) {
                Window.getScene().removeEntity(go);
            }
            selectedObjects.clear();
        }

        if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_CONTROL)) {
            if (Window.keyListener().isKeyPressed(KeyEvent.VK_D)) {
                duplicate();
                debounceKeyLeft = debounceKey;
            }
        }

        if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_R)) {
            rotateObjects(90);
            debounceKeyLeft = debounceKey;
        } else if (debounceKeyLeft <= 0 && Window.keyListener().isKeyPressed(KeyEvent.VK_E)) {
            rotateObjects(-90);
            debounceKeyLeft = debounceKey;
        }


    }

    public void rotateObjects(float degrees) {
        for (Entity go : selectedObjects) {
            go.getTransform().setRotation(go.getTransform().getRotation() + degrees);
            TriangleBounds b = go.getComponent(TriangleBounds.class);
            if (b != null) b.calculateTransform();
        }
    }

    public void moveObjects(Direction direction, float scale) {
        Vector2 distance = new Vector2(0.0f, 0.0f);
        switch (direction) {
            case UP:
                distance.y = -Constants.TILE_HEIGHT * scale;
                break;
            case DOWN:
                distance.y = Constants.TILE_HEIGHT * scale;
                break;
            case LEFT:
                distance.x = -Constants.TILE_WIDTH * scale;
                break;
            case RIGHT:
                distance.x = Constants.TILE_WIDTH * scale;
                break;
            default:
                System.out.println("Error: Direction has no enum '" + direction + "'");
                System.exit(-1);
                break;
        }

        for (Entity go : selectedObjects) {
            go.getTransform().getPosition().x += distance.x;
            go.getTransform().getPosition().y += distance.y;
            float gridX = (float)(Math.floor(go.getTransform().getPosition().x / Constants.TILE_WIDTH) + 1) * Constants.TILE_WIDTH;
            float gridY = (float)(Math.floor(go.getTransform().getPosition().y / Constants.TILE_WIDTH) * Constants.TILE_WIDTH);

            if (go.getTransform().getPosition().x < gridX + 1 && go.getTransform().getPosition().x > gridX - 1) {
                go.getTransform().getPosition().x = gridX;
            }

            if (go.getTransform().getPosition().y < gridY + 1 && go.getTransform().getPosition().y > gridY - 1) {
                go.getTransform().getPosition().y = gridY;
            }

            TriangleBounds b = go.getComponent(TriangleBounds.class);
            if (b != null) b.calculateTransform();
        }
    }

    public void duplicate() {
        for (Entity go : selectedObjects) {
            Window.getScene().addEntity(go.copy());
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (isEditing) {
            Sprite sprite = getEntity().getComponent(Sprite.class);
            if (sprite != null) {
                float alpha = 0.5f;
                AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
                g2.setComposite(ac);
                g2.drawImage(sprite.getImage(), (int) getEntity().getTransform().getPosition().x, (int) getEntity().getTransform().getPosition().y,
                        (int) sprite.getWidth(), (int) sprite.getHeight(), null);
                alpha = 1.0f;
                ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
                g2.setComposite(ac);
            }
        } else if (Window.mouseListener().isMousePressed() &&
                Window.mouseListener().getMouseButton() == MouseEvent.BUTTON1){
            wasDragged = true;
            g2.setColor(new Color(1, 1, 1, 0.3f));
            dragX = Window.mouseListener().getX();
            dragY = Window.mouseListener().getY();
            dragWidth = Window.mouseListener().getDx();
            dragHeight = Window.mouseListener().getDy();
            if (dragWidth < 0) {
                dragWidth *= -1;
                dragX -= dragWidth;
            }
            if (dragHeight < 0) {
                dragHeight *= -1;
                dragY -= dragHeight;
            }
            g2.fillRect((int)dragX, (int)dragY, (int)dragWidth, (int)dragHeight);
        }
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
