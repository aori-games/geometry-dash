package jade.entity.component;

import jade.Window;
import jade.data.Constants;
import jade.entity.Component;
import jade.tool.Camera;

import java.awt.*;
import java.awt.geom.Line2D;

public class Grid extends Component {

    Camera camera;
    private int gridWidth, gridHeight;
    private int numYLines = 31;
    private int numXLines = 20;

    public Grid() {
        this.camera = Window.getWindow().getCurrentScene().getCamera();
        this.gridHeight = Constants.TILE_HEIGHT;
        this.gridWidth = Constants.TILE_WIDTH;
    }

    @Override
    public void update(double dt) {

    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setStroke(new BasicStroke(1f));
        g2.setColor(new Color(0.2f,0.2f,0.2f,0.5f));

        float startX = (float) (Math.floor( camera.getPosition().x / gridWidth) * gridWidth - camera.getPosition().x);
        float startY = (float) (Math.floor( camera.getPosition().y / gridHeight) * gridHeight - camera.getPosition().y);
        float bottom = Math.min(Constants.GROUND_Y - camera.getPosition().y, Constants.SCREEN_HEIGHT);

        for(int column = 0; column <= numYLines; column++) {
            g2.draw(new Line2D.Float(startX, 0 , startX, bottom));
            startX += gridWidth;
        }

        for(int row = 0; row <= numXLines; row++) {
            if(camera.getPosition().y + startY < Constants.GROUND_Y) {
                g2.draw(new Line2D.Float(0, startY , Constants.SCREEN_WIDTH, startY));
                startY += gridHeight;
            }
        }
    }

    // GETTERS & SETTERS
    public int getGridWidth() {
        return gridWidth;
    }

    public void setGridWidth(int gridWidth) {
        this.gridWidth = gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void setGridHeight(int gridHeight) {
        this.gridHeight = gridHeight;
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}
