package jade.entity.component;

import jade.Window;
import jade.entity.Component;
import jade.entity.Entity;
import jade.file.Parser;
import jade.scene.LevelScene;
import jade.scene.Scene;

public class Portal extends Component {

    public PlayerState stateChanger;
    public Entity player;
    private BoxBounds bounds;

    public Portal(PlayerState stateChanger) {
        this.stateChanger = stateChanger;
    }

    public Portal(PlayerState stateChanger, Entity player) {
        this.stateChanger = stateChanger;
        this.player = player;
    }

    @Override
    public void start() {
        this.bounds = getEntity().getComponent(BoxBounds.class);
        Scene scene = Window.getScene();
        if (scene instanceof LevelScene) {
            LevelScene levelScene = (LevelScene) scene;
            this.player = levelScene.getPlayer();
        }
    }

    @Override
    public void update(double dt) {
        if (player != null && player.getComponent(Player.class).playerState != stateChanger && BoxBounds.checkCollision(bounds, player.getComponent(BoxBounds.class))) {
            player.getComponent(Player.class).playerState = stateChanger;
        }
    }

    @Override
    public Component copy() {
        return new Portal(this.stateChanger, this.player);
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder builder = new StringBuilder();
        int state = this.stateChanger.FLYING == this.stateChanger ? 1 : 0;

        builder.append(beginObjectProperty("Portal", tabSize));
        builder.append(addIntProperty("StateChanger", state, tabSize + 1, true, false));
        builder.append(closeObjectProperty(tabSize));
        return builder.toString();
    }

    public static Portal deserialize() {
        int state = Parser.consumeIntProperty("StateChanger");
        PlayerState playerState = state == 1 ? PlayerState.FLYING : PlayerState.NORMAL;
        Parser.consumeEndObjectProperty();
        return new Portal(playerState);
    }
}
