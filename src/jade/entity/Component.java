package jade.entity;

import jade.file.Serialize;

import java.awt.Graphics2D;

public abstract class Component<T> extends Serialize {

    private Entity entity;

    public void update(double dt) {
        return;
    }

    public void draw(Graphics2D g2) {
        return;
    }

    public Component copy() {
        return null;
    };

    public void start() {

    }

    // GETTERS & SETTERS
    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }
}
