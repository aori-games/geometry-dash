package jade.tool;

import util.Vector2;

public class Camera {

    private Vector2 position;

    public Camera(Vector2 position) {

        this.position = position;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }
}
