package jade.tool;

import jade.dataStructure.Transform;
import jade.entity.Entity;
import util.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Render {
    public Map<Integer, List<Entity>> gameObjects;
    Camera camera;

    public Render(Camera camera) {
        this.camera = camera;
        this.gameObjects = new HashMap<>();
    }

    public void submit(Entity gameObject) {
        gameObjects.computeIfAbsent(gameObject.zIndex, k -> new ArrayList<>());
        gameObjects.get(gameObject.zIndex).add(gameObject);
    }

    public void render(Graphics2D g2) {
        int lowestZIndex = Integer.MAX_VALUE;
        int highestZIndex = Integer.MIN_VALUE;
        for (Integer i : gameObjects.keySet()) {
            if (i < lowestZIndex) lowestZIndex = i;
            if (i > highestZIndex) highestZIndex = i;
        }

        int currentZIndex = lowestZIndex;
        while (currentZIndex <= highestZIndex) {
            if (gameObjects.get(currentZIndex) == null) {
                currentZIndex++;
                continue;
            }

            for (Entity g : gameObjects.get(currentZIndex)) {
                if (g.isUI()) {
                    g.draw(g2);
                } else {
                    Transform oldTransform = new Transform(g.getTransform().getPosition());
                    oldTransform.setRotation(g.getTransform().getRotation());
                    oldTransform.setScale(new Vector2(g.getTransform().getScale().x, g.getTransform().getScale().y));

                    g.getTransform().setPosition(new Vector2(g.getTransform().getPosition().x - camera.getPosition().x,
                            g.getTransform().getPosition().y - camera.getPosition().y));
                    g.draw(g2);
                    g.setTransform(oldTransform);
                }
            }

            currentZIndex++;
        }
    }
}
