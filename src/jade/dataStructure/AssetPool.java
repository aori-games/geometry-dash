package jade.dataStructure;

import jade.entity.component.Sprite;
import jade.entity.component.SpriteSheet;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AssetPool {
    static Map<String, Sprite> sprites = new HashMap<String, Sprite>();
    static Map<String, SpriteSheet> spriteSheets = new HashMap<String, SpriteSheet>();

    public static boolean hasSprite (String pictureFile) {
        File tmp = new File(pictureFile);
        return sprites.containsKey(tmp.getAbsolutePath());
    }

    public static boolean hasSpritesheet (String pictureFile) {
        File tmp = new File(pictureFile);
        return AssetPool.spriteSheets.containsKey(tmp.getAbsolutePath());
    }

    public static Sprite getSprite (String pictureFile) {
        File file = new File(pictureFile);
        if(hasSprite(pictureFile)) {
            return sprites.get(file.getAbsolutePath());
        } else {
            Sprite sprite = new Sprite(pictureFile);
            addSprite(pictureFile, sprite);
            return sprites.get(file.getAbsolutePath());
        }
    }

    public static SpriteSheet getSpriteSheet (String pictureFile) {
        File file = new File(pictureFile);
        if(hasSpritesheet(file.getAbsolutePath())) {
            return spriteSheets.get(file.getAbsolutePath());
        }
        System.out.println("Spritesheet : " + pictureFile + " not found");
        System.exit(-1);
        return null;
    }

    /**
     *
     * @param pictureFile The absolute path to the picture
     * @param sprite
     */
     public static void addSprite (String pictureFile, Sprite sprite) {
        File file = new File(pictureFile);
         if(!hasSprite(file.getAbsolutePath())) {
            sprites.put(file.getAbsolutePath(), sprite);
        } else {
             System.out.println("Sprite already exists : " + file.getAbsolutePath());
             System.exit(-1);
         }
    }

    public static void addSpriteSheet (String pictureFile, int tileWidth, int tileHeight, int spacing, int columns, int size) {
         File file = new File(pictureFile);
         if(!hasSpritesheet(file.getAbsolutePath())) {
             SpriteSheet spriteSheet = new SpriteSheet(pictureFile, tileWidth, tileHeight, spacing, columns, size);
             AssetPool.spriteSheets.put(file.getAbsolutePath(), spriteSheet);
         }
    }
}
