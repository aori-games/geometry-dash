package jade.dataStructure;

import jade.file.Parser;
import jade.file.Serialize;
import util.Vector2;

public class Transform extends Serialize {
    private Vector2 position;
    private Vector2 scale;
    private float rotation;

    public Transform(Vector2 position) {
        this.position = position;
        this.scale = new Vector2(1f,1f);
        this.rotation = 0.0f;
    }

    @Override
    public String toString() {
        return "Position (" + position.x + ", " + position.y + ")";
    }

    // GETTERS && SETTERS
    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
    public Vector2 getScale() {
        return scale;
    }

    public void setScale(Vector2 scale) {
        this.scale = scale;
    }

    public Transform copy() {
        Transform transform = new Transform(position.copy());
        transform.scale = this.scale.copy();
        transform.rotation = this.rotation;
        return transform;
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder builder = new StringBuilder();
        builder.append(beginObjectProperty("Transform", tabSize));

        builder.append(beginObjectProperty("Position", tabSize + 1));
        builder.append(position.serialize(tabSize + 2));
        builder.append(closeObjectProperty(tabSize + 1));
        builder.append(addEnding(true, true));

        builder.append(beginObjectProperty("Scale", tabSize + 1));
        builder.append(scale.serialize(tabSize + 2));
        builder.append(closeObjectProperty(tabSize + 1));
        builder.append(addEnding(true, true));

        builder.append(addFloatProperty("Rotation", rotation, tabSize + 1, true, false ));
        builder.append(closeObjectProperty(tabSize + 1));

        return builder.toString();
    }

    public static Transform deserialize() {
        Parser.consumeBeginObjectProperty("Transform");
        Parser.consumeBeginObjectProperty("Position");

        Vector2 position = Vector2.deserialize();
        Parser.consumeEndObjectProperty();
        Parser.consume(',');
        Parser.consumeBeginObjectProperty("Scale");
        Vector2 scale = Vector2.deserialize();
        Parser.consumeEndObjectProperty();
        Parser.consume(',');
        float rotation = Parser.consumeFloatProperty("Rotation");
        Parser.consumeEndObjectProperty();

        Transform transform = new Transform(position);
        transform.setScale(scale);
        transform.setRotation(rotation);
        return transform;

    }
}
