package jade.file;

import jade.entity.Component;
import jade.entity.Entity;
import jade.entity.component.BoxBounds;
import jade.entity.component.Portal;
import jade.entity.component.Sprite;
import jade.entity.component.TriangleBounds;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Parser {
    private static int offset = 0;
    private static int line = 1;
    private static byte[] bytes;

    public static void openFile(String filename) {
        File file = new File("src/levels/" + filename + ".zip");
        if(!file.exists()) {
            return;
        }
        offset = 0;
        line = 1;
        try {
            ZipFile zipFile = new ZipFile("src/levels/" + filename + ".zip");
            ZipEntry jsonFile = zipFile.getEntry(filename + ".json");
            InputStream inputStream = zipFile.getInputStream(jsonFile);
            Parser.bytes = inputStream.readAllBytes();
        } catch (IOException e){
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static boolean atEnd() {
        return offset == bytes.length;
    }

    public static void skipWhiteSpace() {
        while (!atEnd() && (peek() == ' ' || peek() == '\n' || peek() == '\t' || peek() == '\r')) {
            if (peek() == '\n') {
                Parser.line++;
            }
            advance();
        }
    }

    public static char peek() {
        return (char) bytes[offset];
    }

    public static char advance() {
        char c = (char) bytes[offset];
        offset++;
        return c;
    }

    public static void consume(char c) {
        char actual = peek();
        if(actual != c) {
            System.out.print("Error: Expected '" + c + "' but got '" + actual + "' at the line " + Parser.line);
            System.exit(-1);
        }
        offset++;
    }

    public static int parseInt() {
        skipWhiteSpace();
        char c;
        StringBuilder builder = new StringBuilder();

        while (!atEnd() && isDigit(peek()) || peek() == '-') {
            c = advance();
            builder.append(c);
        }
        return Integer.parseInt(builder.toString());
    }

    public static double parseDouble() {
        skipWhiteSpace();
        char c;
        StringBuilder builder = new StringBuilder();
        while (!atEnd() && isDigit(peek()) || peek() == '-' || peek() == '.') {
            c = advance();
            builder.append(c);
        }
        return Double.parseDouble(builder.toString());
    }

    public static float parseFloat() {
        float f = (float) parseDouble();
        consume('f');
        return f;
    }

    public static String parseString() {
        skipWhiteSpace();
        char c;
        StringBuilder builder = new StringBuilder();
        consume('"');
        while (!atEnd() && peek() != '"') {
            c = advance();
            builder.append(c);
        }
        consume('"');
        return builder.toString();
    }

    public static boolean parseBoolean() {
        skipWhiteSpace();
        StringBuilder builder = new StringBuilder();
        if(!atEnd() && peek() == 't') {
            builder.append("true");
            consume('t');
            consume('r');
            consume('u');
            consume('e');
        } else if(peek() == 'f') {
            builder.append("false");
            consume('f');
            consume('a');
            consume('l');
            consume('s');
            consume('e');
        } else {
            System.out.print("Error: Expected 'true' or 'false' but got '" + peek() + "' at the line : " + Parser.line);
            System.exit(-1);
        }
        return Boolean.parseBoolean(builder.toString());
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    public static Component parseComponent() {
        String componentTitle = Parser.parseString();
        skipWhiteSpace();
        Parser.consume(':');
        skipWhiteSpace();
        Parser.consume('{');
        switch (componentTitle) {
            case "Sprite" :
                return Sprite.deserialize();
            case "BoxBounds":
                return BoxBounds.deserialize();
            case "TriangleBounds" :
                return TriangleBounds.deserialize();
            case "Portal" :
                return Portal.deserialize();
            default:
                System.out.println("Could not parse component title: " + componentTitle + " at line : " + Parser.line);
                System.exit(-1);
        }
        return null;
    }

    public static Entity parseEntity() {
        if(bytes.length == 0 || atEnd()) {
            return null;
        }
        if (peek() == ',') {
            Parser.consume(',');
        }
        skipWhiteSpace();
        if(atEnd()) return null;
        return Entity.deserialize();
    }

    public static String consumeStringProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        consume(':');
        return parseString();
    }

    public static int consumeIntProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        consume(':');
        return parseInt();
    }

    public static boolean consumeBooleanProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        consume(':');
        return parseBoolean();
    }

    public static double consumeDoubleProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        consume(':');
        return parseDouble();
    }

    public static float consumeFloatProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        consume(':');
        return parseFloat();
    }

    public static void consumeEndObjectProperty() {
        skipWhiteSpace();
        consume('}');
    }

    public static void consumeBeginObjectProperty(String name) {
        skipWhiteSpace();
        chechString(name);
        skipWhiteSpace();
        consume(':');
        skipWhiteSpace();
        consume('{');
    }

    private static void chechString(String str) {
        String title = Parser.parseString();
        if(title.compareTo(str) != 0) {
            System.out.print("Error: Expected '" + str + "' but got '" + title + "' at the line : " + Parser.line);
            System.exit(-1);
        }

    }
}
