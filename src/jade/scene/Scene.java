package jade.scene;

import jade.entity.Component;
import jade.entity.Entity;
import jade.file.Parser;
import jade.tool.Camera;
import jade.tool.Render;
import util.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public abstract class Scene {
    String name;
    Camera camera;
    public List<Entity> entities;
    public List<Entity> objsToRemove;
    Render renderer;

    public void Scene(String name) {
        this.name = name;
        this.camera = new Camera(new Vector2(0,0));
        this.entities = new ArrayList<>();
        this.renderer = new Render(camera);
        this.objsToRemove = new ArrayList<>();
    }

    public List<Entity> getAllEntities() {
        return entities;
    }

    public void addEntity(Entity e) {
        this.entities.add(e);
        renderer.submit(e);
        for (Component c : e.getAllComponents()) {
            c.start();
        }
    }

    public void removeEntity(Entity e) {
        objsToRemove.add(e);
    }

    public void importLevel(String fileName) {
        for(Entity e : entities) {
            if(!e.serializable) {
                objsToRemove.add(e);
            }
        }

        for(Entity e : objsToRemove) {
            renderer.gameObjects.remove(e.zIndex,e);
            entities.remove(e);
        }
        Parser.openFile(fileName);

        Entity entity = Parser.parseEntity();
        while (entity != null) {
            addEntity(entity);
            entity = Parser.parseEntity();
        }
    }

    public abstract void init();
    public abstract void update(double delta);
    public abstract void draw(Graphics2D g);

    // GETTERS & SETTERS
    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}
