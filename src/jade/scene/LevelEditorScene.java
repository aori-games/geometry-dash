package jade.scene;

import jade.Window;
import jade.data.Constants;
import jade.dataStructure.AssetPool;
import jade.dataStructure.Transform;
import jade.entity.Entity;
import jade.entity.component.*;
import ui.MainContainer;
import util.Vector2;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class LevelEditorScene extends Scene {

    private Entity player;
    private Entity ground;
    private Grid grid;
    private CameraControls cameraControls;
    private Entity mouseCursor;
    private MainContainer editingButtons;

    public LevelEditorScene(String name) {
        super.Scene(name);
    }

    @Override
    public void init() {
        initAssetPool();
        this.editingButtons = new MainContainer();

        grid = new Grid();
        cameraControls = new CameraControls();
        editingButtons.start();

        mouseCursor = new Entity("Mouse Cursor",new Transform(new Vector2()),10);
        mouseCursor.addComponent(new LevelEditorControls(Constants.TILE_WIDTH, Constants.TILE_HEIGHT));

        SpriteSheet layerOne = AssetPool.getSpriteSheet("src/assets/player/layerOne.png");
        SpriteSheet layerTwo = AssetPool.getSpriteSheet("src/assets/player/layerTwo.png");
        SpriteSheet layerThree = AssetPool.getSpriteSheet("src/assets/player/layerThree.png");
        Player playerComp = new Player(layerOne.getSprites().get(0),layerTwo.getSprites().get(0),layerThree.getSprites().get(0), Color.RED, Color.GREEN);
        player = new Entity("Some game object",new Transform(new Vector2(500,350.0f)),0);
        player.addComponent(playerComp);

        player.setNoneSerializable();
        addEntity(player);
        initBackground();
    }

    public void initAssetPool() {
        AssetPool.addSpriteSheet("src/assets/player/layerOne.png",42,42,2,13,13*5);
        AssetPool.addSpriteSheet("src/assets/player/layerTwo.png",42,42,2,13,13*5);
        AssetPool.addSpriteSheet("src/assets/player/layerThree.png",42,42,2,13,13*5);
        AssetPool.addSpriteSheet("src/assets/groundSprites.png", 42, 42, 2, 6, 12);
        AssetPool.addSpriteSheet("src/assets/ui/buttonSprites.png", 60, 60, 2, 2, 2);
        AssetPool.addSpriteSheet("src/assets/ui/tabs.png", Constants.TAB_WIDTH, Constants.TAB_HEIGHT,2,6,6);
        AssetPool.addSpriteSheet("src/assets/spikes.png",42,42,2,6,4);
        AssetPool.addSpriteSheet("src/assets/bigSprites.png",84,84,2,2,2);
        AssetPool.addSpriteSheet("src/assets/smallBlocks.png",42,42,2,6,1);
        AssetPool.addSpriteSheet("src/assets/portal.png",44,85,2,2,2);
    }

    @Override
    public void update(double delta) {

        //IT STICKS TO THE GROUND
        if(camera.getPosition().y > Constants.CAMERA_OFFSET_GROUND_Y + 70) {
            camera.getPosition().y = Constants.CAMERA_OFFSET_GROUND_Y + 70;
        }
        for(Entity e : entities) {
            e.update(delta);
        }
        cameraControls.update(delta);
        grid.update(delta);
        editingButtons.update(delta);
        mouseCursor.update(delta);

        if(Window.getWindow().getKeyListener().isKeyPressed(KeyEvent.VK_F1)) {
            export("test");
        } else if(Window.getWindow().getKeyListener().isKeyPressed(KeyEvent.VK_F2)) {
            importLevel("test");
        } else  if(Window.getWindow().getKeyListener().isKeyPressed(KeyEvent.VK_F3)) {
            Window.getWindow().changeScene(1);
        }
        if(!objsToRemove.isEmpty()) {
            for (Entity entity : objsToRemove) {
                entities.remove(entity);
                renderer.gameObjects.get(entity.zIndex).remove(entity);
            }
            objsToRemove.clear();
        }
    }
    public void initBackground() {
        ground = new Entity("Ground",new Transform(new Vector2(0,Constants.GROUND_Y)),1);
        ground.addComponent(new Ground());
        ground.setNoneSerializable();
        addEntity(ground);

        int numBackground = 5;
        Entity[] backgrounds = new Entity[numBackground];
        Entity[] groundsBgs = new Entity[numBackground];
        for (int i = 0; i < numBackground; i++) {
            ParallaxBackground bg = new ParallaxBackground("src/assets/backgrounds/bg01.png",null, ground.getComponent(Ground.class), false);
            int x = i * bg.sprite.getWidth();
            int y = 0;

            Entity backgroundEntity = new Entity("Background", new Transform(new Vector2(x, y)),-10);
            backgroundEntity.setUI(true);
            backgroundEntity.setNoneSerializable();
            backgroundEntity.addComponent(bg);
            backgrounds[i] = backgroundEntity;

            ParallaxBackground groundsBg = new ParallaxBackground("src/assets/grounds/ground01.png", null,ground.getComponent(Ground.class), true);
            x = i * bg.sprite.getWidth();
            y = (int) ground.getTransform().getPosition().y;
            Entity groundEntity = new Entity("GroundBg", new Transform(new Vector2(x, y)),-9);
            groundEntity.addComponent(groundsBg);
            groundEntity.setUI(true);
            groundEntity.setNoneSerializable();
            groundsBgs[i] = groundEntity;

            addEntity(groundEntity);
            addEntity(backgroundEntity);
        }
    }

    private void export(String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream("src/levels/" + fileName + ".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry( fileName + ".json"));
            int i = 0;
            for(Entity e : entities) {
                String str = e.serialize(0);
                if(str.compareTo("") != 0) {
                    zos.write(str.getBytes());
                    if(i != entities.size() - 1) {
                        zos.write(",\n".getBytes());
                    }
                }
                i++;
            }
            zos.closeEntry();
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Constants.BG_COLOR);
        g2.fillRect(0,0,Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT);


        renderer.render(g2);
        grid.draw(g2);
        editingButtons.draw(g2);
        mouseCursor.draw(g2);
    }

    // GETTERS & SETTERS
    public Entity getMouseCursor() {
        return mouseCursor;
    }
    public void setMouseCursor(Entity mouseCursor) {
        this.mouseCursor = mouseCursor;
    }
}
