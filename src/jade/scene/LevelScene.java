package jade.scene;

import jade.data.Constants;
import jade.dataStructure.AssetPool;
import jade.dataStructure.Transform;
import jade.entity.Entity;
import jade.entity.component.*;
import util.Vector2;

import java.awt.*;

public class LevelScene extends Scene {

    private Entity player;
    private Entity ground;
    private BoxBounds playerBounds;

    public LevelScene(String name) {
        super.Scene(name);
    }

    @Override
    public void init() {
        initAssetPool();

        SpriteSheet layerOne = AssetPool.getSpriteSheet("src/assets/player/layerOne.png");
        SpriteSheet layerTwo = AssetPool.getSpriteSheet("src/assets/player/layerTwo.png");
        SpriteSheet layerThree = AssetPool.getSpriteSheet("src/assets/player/layerThree.png");
        Player playerComp = new Player(layerOne.getSprites().get(0), layerTwo.getSprites().get(0), layerThree.getSprites().get(0), Color.RED, Color.GREEN);
        playerBounds = new BoxBounds(Constants.TILE_WIDTH - 2, Constants.TILE_HEIGHT - 2);
        player = new Entity("Some game object", new Transform(new Vector2(500, 350.0f)), 0);
        player.addComponent(playerComp);
        player.addComponent(new RigidBody(new Vector2(Constants.PLAYER_SPEED, 0)));
        player.addComponent(playerBounds);

        renderer.submit(player);

        initBackground();

        importLevel("test");
    }

    public void initBackground() {
        ground = new Entity("Ground", new Transform(new Vector2(0, Constants.GROUND_Y)), 1);
        ground.addComponent(new Ground());
        addEntity(ground);

        int numBackground = 7;
        Entity[] backgrounds = new Entity[numBackground];
        Entity[] groundsBgs = new Entity[numBackground];
        for (int i = 0; i < numBackground; i++) {
            ParallaxBackground bg = new ParallaxBackground("src/assets/backgrounds/bg01.png", backgrounds, ground.getComponent(Ground.class), false);
            int x = i * bg.sprite.getWidth();
            int y = 0;

            Entity backgroundEntity = new Entity("Background", new Transform(new Vector2(x, y)), -10);
            backgroundEntity.setUI(true);
            backgroundEntity.addComponent(bg);
            backgrounds[i] = backgroundEntity;

            ParallaxBackground groundsBg = new ParallaxBackground("src/assets/grounds/ground01.png", groundsBgs, ground.getComponent(Ground.class), true);
            x = i * bg.sprite.getWidth();
            y = 0;
            Entity groundEntity = new Entity("GroundBg", new Transform(new Vector2(x, y)), -9);
            groundEntity.addComponent(groundsBg);
            groundEntity.setUI(true);
            groundsBgs[i] = groundEntity;

            addEntity(groundEntity);
            addEntity(backgroundEntity);
        }
    }

    public void initAssetPool() {
        AssetPool.addSpriteSheet("src/assets/player/layerOne.png", 42, 42, 2, 13, 13 * 5);
        AssetPool.addSpriteSheet("src/assets/player/layerTwo.png", 42, 42, 2, 13, 13 * 5);
        AssetPool.addSpriteSheet("src/assets/player/layerThree.png", 42, 42, 2, 13, 13 * 5);
        AssetPool.addSpriteSheet("src/assets/groundSprites.png", 42, 42, 2, 6, 12);
        AssetPool.getSprite("src/assets/player/spaceship.png");
    }

    @Override
    public void update(double delta) {
        //IF THE PLAYER'S MOVING THEN CAMERA FOLLOWS IN X
        if (player.getTransform().getPosition().x - camera.getPosition().x > Constants.CAMERA_OFFSET_X) {
            camera.getPosition().x = player.getTransform().getPosition().x - Constants.CAMERA_OFFSET_X;
        }

        camera.getPosition().y = player.getTransform().getPosition().y - Constants.CAMERA_OFFSET_Y;

        //IT STICKS TO THE GROUND
        if (camera.getPosition().y > Constants.CAMERA_OFFSET_GROUND_Y) {
            camera.getPosition().y = Constants.CAMERA_OFFSET_GROUND_Y;
        }

        player.update(delta);
        player.getComponent(Player.class).setOnGround(false);
        for (Entity e : entities) {
            e.update(delta);
            Bounds b = e.getComponent(Bounds.class);
            if (b != null) {
                if (Bounds.checkCollision(playerBounds, b)) {
                    Bounds.resolveCollision(b, player);
                }
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Constants.BG_COLOR);
        g2.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);


        renderer.render(g2);
    }

    // GETTERS & SETTERS
    public Entity getPlayer() {
        return player;
    }
}
